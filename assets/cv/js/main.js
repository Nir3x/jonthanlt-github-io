jQuery(document).ready(function ($) {

    $('.level-bar-inner').css('width', '0');

    $(window).on('load', function () {

        $('.level-bar-inner').each(function () {

            var itemWidth = $(this).data('level');

            $(this).animate({
                width: itemWidth
            }, 800);

        });

    });

    $('#nbMonth').html(function () {
        var today = new Date();
        var limitFive = new Date(2018, 11);
        limitFive.setDate(today.getDate());
        var testedDate = new Date();
        testedDate.setDate(today.getDate());
        var jourSep = testedDate - limitFive;

        return(Math.round(jourSep / 86400000 / 30 + 1))
    });

});
